# base_module

This is a utility package that helps the developer to easily create new screens and view which are empowered with the abilities of MVP project structure and Provide State Management structure.
Along with this, the base classes such as `BaseScreen` and `BaseView` has essential properties that can be used inorder to create a new screen and a single view.

While creating a new screen the developer will require to add the title, the action buttons, bottom navigation bar, etc. which can be created as a preset and ready to use out of the box. As required the widgets must be customised in the base classes.
