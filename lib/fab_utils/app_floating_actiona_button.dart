import 'package:flutter/material.dart';

class AppFloatingActionButton {
  final FloatingActionButton button;
  final FloatingActionButtonLocation? location;
  final FloatingActionButtonThemeData? themeData;

  AppFloatingActionButton({required this.button, this.location, this.themeData});
}
