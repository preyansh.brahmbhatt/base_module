import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'base_module_platform_interface.dart';

/// An implementation of [BaseModulePlatform] that uses method channels.
class MethodChannelBaseModule extends BaseModulePlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('base_module');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
