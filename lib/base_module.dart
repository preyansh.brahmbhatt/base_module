
import 'base_module_platform_interface.dart';

class BaseModule {
  Future<String?> getPlatformVersion() {
    return BaseModulePlatform.instance.getPlatformVersion();
  }
}
