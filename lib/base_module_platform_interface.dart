import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'base_module_method_channel.dart';

abstract class BaseModulePlatform extends PlatformInterface {
  /// Constructs a BaseModulePlatform.
  BaseModulePlatform() : super(token: _token);

  static final Object _token = Object();

  static BaseModulePlatform _instance = MethodChannelBaseModule();

  /// The default instance of [BaseModulePlatform] to use.
  ///
  /// Defaults to [MethodChannelBaseModule].
  static BaseModulePlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [BaseModulePlatform] when
  /// they register themselves.
  static set instance(BaseModulePlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
