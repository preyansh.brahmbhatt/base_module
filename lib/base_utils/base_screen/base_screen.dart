import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../fab_utils/app_floating_actiona_button.dart';
import '../base_presenter.dart';
import '../base_provider.dart';
import '../base_view/base_view.dart';
import 'base_screen_builder.dart';

abstract class BaseScreen<PV extends BaseProvider, P extends BasePresenter>
    extends StatelessWidget {
  late final PV baseProvider;
  late final P basePresenter;
  late final BuildContext baseContext;

  BaseScreen({super.key}) {
    return;
  }

  PV setProvider();

  P setPresenter();

  BaseView createView();

  Widget? createScaffoldView() => null;

  String? get title => "base_screen";

  bool get hasAppBar => true;

  AppBar? get customAppBar => null;

  List<Widget> get actionButtons => [];

  AppFloatingActionButton? get appFloatingActionButton => null;

  List<NavigationDestination>? get bottomNavBarItems => null;

  Widget? get bottomAppBar => null;

  @override
  Widget build(BuildContext context) {
    baseContext = context;
    return ChangeNotifierProvider<PV>(
      create: (context) => setProvider(),
      builder: (context, child) => BaseScreenBuilder<PV>(
        this,
        (provider) {
          baseProvider = provider;
          basePresenter = setPresenter()
            ..provider = provider
            ..context = context;
        },
      ),
    );
  }
}
