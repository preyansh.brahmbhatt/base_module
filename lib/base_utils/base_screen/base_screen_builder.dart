import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../base_provider.dart';
import '../base_view/base_view.dart';
import '../base_view/base_view_pro.dart';
import 'base_screen.dart';

class BaseScreenBuilder<PV extends BaseProvider> extends StatelessWidget {
  final BaseScreen baseScreen;
  final Function(PV provider) setProvider;

  const BaseScreenBuilder(this.baseScreen, this.setProvider, {super.key});

  @override
  Widget build(BuildContext context) {
    PV provider = Provider.of<PV>(context, listen: false);
    setProvider(provider);
    BaseView baseView;
    if (baseScreen.createView() is BaseViewPro) {
      baseView = baseScreen.createView() as BaseViewPro
        ..baseContext = baseScreen.baseContext
        ..bsProvider = baseScreen.baseProvider
        ..bsPresenter = baseScreen.basePresenter;
    } else {
      baseView = baseScreen.createView()..baseContext = baseScreen.baseContext;
    }

    return baseScreen.createScaffoldView() ??
        Scaffold(
          appBar: getAppBar(context),
          body: baseView,
          floatingActionButton: baseScreen.appFloatingActionButton?.button,
          floatingActionButtonLocation:
              baseScreen.appFloatingActionButton?.location,
          bottomNavigationBar: getBottomNavigationBar(),
        );
  }

  Widget? getBottomNavigationBar() {
    return baseScreen.bottomNavBarItems != null
        ? Consumer<PV>(
            builder: (context, value, child) => NavigationBar(
                  onDestinationSelected: (int index) =>
                      value.updateNavigationIndex(index),
                  selectedIndex: value.selectedNavigationIndex,
                  destinations: baseScreen.bottomNavBarItems!,
                ))
        : baseScreen.bottomAppBar;
  }

  AppBar? getAppBar(BuildContext context) {
    if (!baseScreen.hasAppBar) {
      return null;
    }
    return baseScreen.customAppBar ??
        AppBar(
          title: baseScreen.title != null ? Text(baseScreen.title!) : null,
          actions: const [],
        );
  }
}
