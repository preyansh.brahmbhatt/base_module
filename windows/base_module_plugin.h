#ifndef FLUTTER_PLUGIN_BASE_MODULE_PLUGIN_H_
#define FLUTTER_PLUGIN_BASE_MODULE_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

namespace base_module {

class BaseModulePlugin : public flutter::Plugin {
 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrarWindows *registrar);

  BaseModulePlugin();

  virtual ~BaseModulePlugin();

  // Disallow copy and assign.
  BaseModulePlugin(const BaseModulePlugin&) = delete;
  BaseModulePlugin& operator=(const BaseModulePlugin&) = delete;

  // Called when a method is called on this plugin's channel from Dart.
  void HandleMethodCall(
      const flutter::MethodCall<flutter::EncodableValue> &method_call,
      std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);
};

}  // namespace base_module

#endif  // FLUTTER_PLUGIN_BASE_MODULE_PLUGIN_H_
