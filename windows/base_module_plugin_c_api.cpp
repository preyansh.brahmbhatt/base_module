#include "include/base_module/base_module_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "base_module_plugin.h"

void BaseModulePluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  base_module::BaseModulePlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
