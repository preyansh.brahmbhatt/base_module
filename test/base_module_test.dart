import 'package:flutter_test/flutter_test.dart';
import 'package:base_module/base_module.dart';
import 'package:base_module/base_module_platform_interface.dart';
import 'package:base_module/base_module_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockBaseModulePlatform
    with MockPlatformInterfaceMixin
    implements BaseModulePlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final BaseModulePlatform initialPlatform = BaseModulePlatform.instance;

  test('$MethodChannelBaseModule is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelBaseModule>());
  });

  test('getPlatformVersion', () async {
    BaseModule baseModulePlugin = BaseModule();
    MockBaseModulePlatform fakePlatform = MockBaseModulePlatform();
    BaseModulePlatform.instance = fakePlatform;

    expect(await baseModulePlugin.getPlatformVersion(), '42');
  });
}
