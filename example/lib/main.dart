import 'package:base_module/base_utils/base_presenter.dart';
import 'package:base_module/base_utils/base_provider.dart';
import 'package:base_module/base_utils/base_screen/base_screen.dart';
import 'package:base_module/base_utils/base_view/base_view.dart';
import 'package:base_module/fab_utils/app_floating_actiona_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApplication());
}

class MyApplication extends StatelessWidget {
  const MyApplication({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
      ),
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends BaseScreen<HomeProvider, HomePresenter> {
  @override
  HomePresenter setPresenter() => HomePresenter();

  @override
  HomeProvider setProvider() => HomeProvider();

  @override
  AppBar? get customAppBar => AppBar(
    title: Consumer<HomeProvider>(
      builder: (context, provider, child) => Text(
          provider.selectedNavigationIndex == 0
              ? "Welcome Home"
              : "Welcome to Work"),
    ),
  );

  @override
  AppFloatingActionButton? get appFloatingActionButton =>
      AppFloatingActionButton(
          button: FloatingActionButton(
            elevation: 0,
            onPressed: () => basePresenter.showAlertDialog(),
            child: const Icon(Icons.add),
          ),
          location: FloatingActionButtonLocation.centerDocked);

  @override
  BaseView createView() {
    return HomeView();
  }

  @override
  List<NavigationDestination>? get bottomNavBarItems => [
    const NavigationDestination(
        selectedIcon: Icon(Icons.home),
        icon: Icon(Icons.home_outlined),
        label: "Home"),
    const NavigationDestination(
        selectedIcon: Icon(Icons.home_repair_service_rounded),
        icon: Icon(Icons.home_repair_service_outlined),
        label: "Work"),
  ];
}

class HomeView extends BaseView {
  @override
  Widget createView() {
    return const Card(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "This is the view of a Card.",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),
                ),
              ],
            ),
            Divider(
              thickness: 2,
            ),
            Text("4 Days"),
            SizedBox(height: 4),
            Text(
                "This is just a simple text to let you know about the problem you have in your system."),
          ],
        ),
      ),
    );
  }
}

class HomeProvider extends BaseProvider {
  int count = 0;

  void updateCount(int count) {
    this.count = count;
    notifyListeners();
  }
}

class HomePresenter extends BasePresenter<HomeProvider> {
  showAlertDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text("ALERT!!!"),
        content: const Text(
            "This is the sample dialog which explains you how the dialog is shown."),
        actions: [
          FilledButton(
              onPressed: () => Navigator.pop(context),
              child: const Text("Okay"))
        ],
      ),
    );
  }
}